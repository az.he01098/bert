#!/bin/bash
# polyFreeWorld - Creates a PLC for a flat world with electrodes
#     can be used for all electrode arrays including crosshole/hole2surface
#     usually called from the BERT cfg script by
#     PARAGEOMETRY="source polyFlatWorld $DATAFILE"
#     in the command domain such that other cfg options can be passed
#     it tries to interpret PARADEPTH, PARABOUNDARY, BOUNDARY and 
#     PARADX (here absolute not relative!)
[ $# -eq 0 ] && echo "usage: polyFreeWorld datafile size" && exit
datafile=$1
echo POLYFREEWORLD $datafile
[ ! -f "$datafile" ] && echo could not read data file $datafile && exit
# standard parameters
[ -z "$BOUNDARY" ] && BOUNDARY=500
[ -z "$PARABOUNDARY" ] && PARABOUNDARY=5
[ -z "$PARADEPTH" ] && PARADEPTH=0
[ -z "$PARAMAXCELLSIZE" ] && PARAMAXCELLSIZE=0
[ -z "$PARADX" ] && PARADX=0
[ $PARADEPTH == 0.11 ] && PARADEPTH=0
#[ "$PARADEPTH" == "${PARADEPTH#-}" ] && PARADEPTH=-$PARADEPTH
# initialization
mkdir -p mesh
MESH=mesh/mesh
read line < $datafile # extract first line
#nel=${line%#*} # number of electrodes
nel=`echo ${line%#*}|tr -d "\r" ` # number of electrodes, remove CR first
echo $nel
[ -z "$nel" ] && could not read data file $datafile && return
echo Reading $datafile with $nel electrodes
head -n $[ nel + 2 ] < $datafile |tail -n $nel > elec.xz
dim=`head -n1 elec.xz|wc -w`
# detect minimum and maximum electrode positions
eminx=`sort -n -k1 < elec.xz|head -n1|awk '{ print $1}'`
emaxx=`sort -n -k1 < elec.xz|tail -n1|awk '{ print $1}'`
eminy=`sort -n -k2 < elec.xz|head -n1|awk '{ print $2}'`
emaxy=`sort -n -k2 < elec.xz|tail -n1|awk '{ print $2}'`
[ $dim -eq 2 ] && edep=`sort -n -k$dim < elec.xz|head -n1|awk '{ print $2}'`
[ $dim -eq 3 ] && edep=`sort -n -k$dim < elec.xz|head -n1|awk '{ print $3}'`
[ ! "$edep" == "${edep#-}" ] && edep=${edep#-}
# adds a boundary around the min/max positions
addx=`echo $eminx $emaxx $PARABOUNDARY |awk '{ print ($2-$1)*$3/100 }'`
addy=`echo $eminy $emaxy $PARABOUNDARY |awk '{ print ($2-$1)*$3/100 }'`
xmin=`echo $eminx $addx | awk '{ print $1-$2 }'`
xmax=`echo $emaxx $addx | awk '{ print $1+$2 }'`
ymin=`echo $eminy $addy | awk '{ print $1-$2 }'`
ymax=`echo $emaxy $addy | awk '{ print $1+$2 }'`
xmean=`echo $eminx $emaxx| awk '{ print ($1+$2)/2 }'`
ymean=`echo $eminy $emaxy| awk '{ print ($1+$2)/2 }'`
zmean=`echo $edep | awk '{ print $1/2 }'`
echo eminx=$eminx emaxx=$emaxx eminy=$eminy emaxy=$emaxy BOUNDARY=$BOUNDARY
SIZE=`echo $eminx $emaxx $eminy $emaxy $BOUNDARY| awk '{print ($2+$4-$1-$3)*($5/100+1)}'`
[ $PARADEPTH == 0 ] && PARADEPTH=`echo $edep $addx | awk '{ print ($1+$2) }'`
ZSIZE=`echo $eminx $emaxx $eminy $emaxy $BOUNDARY $PARADEPTH| awk '{print ($2+$4-$1-$3)*$5/100+$6}'`
# create world (outer space with marker 1 for background)
echo SIZE=$SIZE/$ZSIZE PARABOUNDARY=$PARABOUNDARY PARADEPTH=$PARADEPTH CELLSIZE=$PARAMAXCELLSIZE
echo edep=$edep add=$addx
# 2D case
if [ $dim -eq 2 ]
then
  polyCreateWorld -v -m1 -d$dim -x $SIZE -z $ZSIZE $MESH
  echo Determined outer limits x=$xmin..$xmax z=0..$PARADEPTH zmean=$zmean
  #polyScale -y 0.5 $MESH # z half as deep as x
  polyTranslate -x $xmean $MESH
  file=int.xz
  echo $xmax 0 > $file
  echo $xmax -$PARADEPTH >> $file
  echo $xmin -$PARADEPTH >> $file
  echo $xmin 0 >> $file
  polyAddProfile -i $file $MESH
  polyAddVIP -x $xmean -y -0.01 -m 2 -a $PARAMAXCELLSIZE -R $MESH
#  if [ ! "$PARADX" == 0 ]
#  then
#    cat elec.xz |awk '{ printf "%s %s\n",$1,$2-'$PARADX'}' > dz.xz
#    polyAddVIP -f dz.xz $MESH
#    rm dz.xz
#  fi
  rm -f int.xz
fi
# 3D case
if [ $dim -eq 3 ]
then
  polyCreateWorld -v -m1 -d$dim -x $SIZE -y $SIZE -z $SIZE $MESH
  echo Determined outer limits x=$xmin..$xmax y=$ymin..$ymax z=0..$PARADEPTH
  polyScale -z 0.5 $MESH # z half as deep as x
  polyTranslate -x $xmean -y$ymean $MESH
  dx=`echo $xmax $xmin|awk '{ print $1-$2 }'`
  dy=`echo $ymax $ymin|awk '{ print $1-$2 }'`
  polyCreateCube -m2 -a $PARAMAXCELLSIZE cube
  polyTranslate -x 0.5 -y 0.5 -z -0.5 cube
  polyScale -x $dx -y $dy -z $PARADEPTH cube
  polyTranslate -x $xmin -y $ymin cube
  polyMerge $MESH cube $MESH
fi
# finally export PLC to mesh/mesh-poly.vtk
polyConvert -V -o $MESH-poly $MESH
# use paraview --data=mesh/mesh-poly.vtk & to see PLC (Wireframe make+Glyph)
