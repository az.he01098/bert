This example shows how to deal with a given topography in form of a digital elevation model (DEM).

We chose one DEM from a 3x4 km region in a mountainous region and looking for the topography effect of a measurement of a dipole-dipole experiment with 17 electrodes and 200m dipole length in y-direction.

Files:
    calc.sh -- the main calculation script, perform the mesh generation and finite element calculation

    topo.xyz -- digital elevation model (DEM)

    dipole17.shm -- configuration for Dipole-Dipole measurement with 17 electrodes
