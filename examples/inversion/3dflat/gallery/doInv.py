#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import os
import numpy as np
import pygimli as pg
from pygimli.physics import ert


def createMixMesh(data, dx=None, verbose=True):
    """Create a triangle prism mesh."""
    dx = dx or data.sensor(0).distance(data.sensor(1))
    ex = pg.x(data)
    ey = pg.y(data)
    paraBoundary = dx
    boundary = dx * 20
    depth = 10
    x = np.arange(min(ex)-paraBoundary, max(ex)+paraBoundary+.01, dx)
    y = np.arange(min(ey)-paraBoundary, max(ey)+paraBoundary+.01, dx)
    grid = pg.createGrid(x, y)

    for c in grid.cells():
        c.setMarker(2)

    pg.show(grid, markers=True)

    mesh2 = pg.meshtools.appendTriangleBoundary(grid,
                                                xbound=boundary,
                                                ybound=boundary,
                                                quality=30,
                                                marker=1, isSubSurface=False)

    mesh2.smooth(True, False, 1, 2)

    for b in mesh2.boundaries():
        if b.marker() < 0:
            b.setMarker(pg.core.MARKER_BOUND_MIXED)

    pg.show(mesh2, markers=True)

    mesh3 = pg.meshtools.createMesh3D(
        mesh2,
        -pg.cat(np.arange(0, 12.6, 2.5/2),
                pg.utils.grange(2.5, boundary, n=10) + 12.5),
        pg.core.MARKER_BOUND_HOMOGEN_NEUMANN,
        pg.core.MARKER_BOUND_MIXED)

    for c in mesh3.cells():
        if c.marker() == 2:
            if c.center()[2] < -depth:
                c.setMarker(1)

    return mesh3


if __name__ == "__main__":
    data = pg.load("gallery.dat")
    pg.getExampleFile("ert/gallery3d.dat")
    mesh = createMixMesh(data)
    mesh.exportVTK("mesh.vtk")
    data["err"] = ert.estimateError(data, relativeError=0.02)
    mgr = ert.ERTManager(data)
    mgr.invert(mesh=mesh, verbose=True)
    mgr.saveResult()
    # %%
    pg.show(mgr.paraDomain, mgr.model, notebook=False)
    # for z in