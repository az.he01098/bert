// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <domain3d.h>
using namespace DCFEMLib;
using namespace MyMesh;

#include <dcfemlib.h>
#include <longoptions.h>
#include <iostream>

using namespace std;


int main( int argc, char * argv[] ){

  LongOptionsList lOpt;
  lOpt.setLastArg("[-HZs:x:y:z:m:a:] polygon-filename");
  lOpt.setDescription( (string) "Creates a unit cube" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "isHole", no_argument, 'H', "bool: Hole marker for this cube" );
  lOpt.insert( "close", no_argument, 'O', "bool: Close cube (top and bottom)" );
  lOpt.insert( "cylinder", no_argument, 'Z', "Create a cylinder instead of a cube" );
  lOpt.insert( "segments", required_argument, 's', "cylinder segments [12]" );
  lOpt.insert( "x", required_argument, 'x', "double: x-pos [ 0.0 ]" );
  lOpt.insert( "y", required_argument, 'y', "double: y-pos [ 0.0 ]" );
  lOpt.insert( "z", required_argument, 'z', "double: z-pos [ 0.0 ]" );
  lOpt.insert( "marker", required_argument, 'm', "int: Region marker [1]" );
  lOpt.insert( "area", required_argument, 'a', "double: max size of tetrahedron [off == 0.0]" );

  bool help = false, verbose = false, hole = false, cylinder = false, close = true;
  int marker = 0;
  double xdim = 0.0, ydim = 0.0, zdim = 0.0, area = 0.0;
  int option_char = 0, option_index = 0, tracedepth = 0, nSegments = 12;
  while ( ( option_char = getopt_long( argc, argv, "?hvHZO"
				       "x:y:z:m:a:s:", lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    case 'Z': cylinder = true; break;
    case 'H': hole = true; break;
    case 'O': close = false; break;
    case 'x': xdim=atof( optarg ); break;
    case 'y': ydim=atof( optarg ); break;
    case 'z': zdim=atof( optarg ); break;
    case 'm': marker=atoi( optarg ); break;
    case 'a': area=atof( optarg ); break;
    case 's': nSegments=atoi( optarg ); break;
    default : cerr << "default value not defined" << endl;
    }
  }
  if ( argc < 2 ) { lOpt.printHelp( argv[0] ); return 1; }

  string domainName( argv[argc-1] );
  string domainFileName( domainName.substr( 0, domainName.rfind( ".poly" ) ) + ".poly" );

  if ( cylinder ){
    createCylinder( RealPos( xdim, ydim, zdim ), marker, nSegments, area, hole, close ).saveTetgenPolyFile( domainFileName, true );
  } else{
    createCube( RealPos( xdim, ydim, zdim ), marker, area, hole, close ).saveTetgenPolyFile( domainFileName );
  }
  return 0;

}
