// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <ctime>
#include <dcfemlib.h>
using namespace DCFEMLib;

#include <mesh3d.h>
#include <domain2d.h>
#include <domain3d.h>
using namespace MyMesh;

#include <longoptions.h>

#include <iostream>
#include <fstream>
#include <cstdio>

using namespace std;

int main( int argc, char *argv[] ){
//  clock();

  LongOptionsList lOpt;
  lOpt.setLastArg( "[-x] mesh" );
  lOpt.setDescription( "Create node-file for local meshrefinement with TetGen" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "dx", required_argument, 'x', "Refinement distance relative to domain center [ 0.1 ]" );
  lOpt.insert( "dr", required_argument, 'r', "Refinement distance relative to electrode center [ 0.0 ]" );
  lOpt.insert( "output", required_argument, 'o', "output filename [mesh.a.node]" );


  //** define default values
  bool help = false, verbose = false;
  double dx = 0.1, dr = 0.0;
  string outputName = NOT_DEFINED;

  int option_char = 0, option_index;
  while ( ( option_char = getopt_long( argc, argv, "?"
                                       "h"
                                       "v"
                                       "x:" // dx
                                       "r:" // dx
                                       "o:"
                                       , lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    case 'x': dx = atof( optarg ); break;
    case 'r': dr = atof( optarg ); break;
    case 'o': outputName = optarg; break;
    default : cerr << WHERE_AM_I << " undefined option: " << (char)option_char << endl;
    }
  }

  bool polyFile = false;

  string meshname;
  if ( argc < 2 ) {
    lOpt.printHelp( argv[ 0 ] );
    exit( 1 );
  } else {
    meshname = argv[ argc - 1 ];
  }

  if ( outputName == NOT_DEFINED ){
    if ( meshname.find( ".poly" ) != (uint)-1 ) {
      polyFile = true;
      outputName = meshname.substr( 0, meshname.rfind( ".poly" ) ) + ".a.node";
    } else {
      outputName = meshname.substr( 0, meshname.rfind( ".bms" ) ) + ".a.node";
    }
  }

  vector < RealPos > addNodes;

  if ( polyFile ){

    int dimension = findDomainDimension( meshname );

    BaseMesh *world;
    switch ( dimension ){
    case 2:
      world = new Domain2D();
      break;
    case 3:
      world = new Domain3D();
      break;
    default: return -1;
    }

    world->load( meshname );
    vector < int > sources( world->getAllMarkedNodes( -99 ) );

    if ( dimension == 2 ){
      RealPos averageNegNorm( 0.0, 0.0, 0.0 );
      int normCount = 0;
      for ( uint i = 0; i < sources.size(); i ++ ){
        SetpCells faceSet( world->node( sources[ i ] ).boundarySet() );
        normCount = 0;
        for ( SetpCells::const_iterator it = faceSet.begin(); it != faceSet.end(); it ++ ){
          if ( (*it)->marker() < 0 ){
            normCount ++;
            averageNegNorm += (*it)->normVector();
            // cout <<  averageNegNorm << " " << (*it)->normVector() << endl;
          }
        }
        addNodes.push_back( world->node( sources[ i ] ).pos() + averageNegNorm / (double)normCount * dx );
      }
    } else {
      TO_IMPL
    }

    fstream file; openOutFile( outputName, &file );
    for ( uint i = 0; i < addNodes.size(); i ++ ){
      if ( dimension == 2 ) {
        file << addNodes[ i ][ 0 ] << "\t" <<  addNodes[ i ][ 1 ] << endl;
      } else if ( dimension == 3 ){
        file << addNodes[ i ][ 0 ] << "\t" <<  addNodes[ i ][ 1 ] << "\t" << addNodes[ i ][ 2 ] << endl;
      }
    }
    file.close();
  } else { // if no poly file
    Mesh3D mesh;
    mesh.loadTetGenMesh( meshname );
    if ( verbose ) mesh.showInfos();

    RealPos averagePos;
    if ( fabs( dr ) > 0.0 ) {
      if ( verbose ) cout << "Refine dr in electrode center direction = " << dr << endl;
      dx = dr;
      averagePos = averagePosition( mesh.positions(  mesh.getAllMarkedNodes( -99 ) ) );
    } else {
      if ( verbose ) cout << "Refine dx in mesh center direction = " << dx << endl;
      averagePos = averagePosition( mesh.nodePositions() );
    }

    for ( int i = 0; i < mesh.nodeCount(); i ++ ){
     if ( mesh.node( i ).marker() == -99 || mesh.node( i ).marker() == -999 ){
        SetpCells bounds( mesh.node( i ).boundarySet() );
         RealPos averageNorm( 0.0, 0.0, 0.0 );
          uint count = 0;
          for ( SetpCells::iterator it = bounds.begin(); it != bounds.end(); it ++ ){
             if ( (*it)->marker() != 0 ){
             averageNorm += (*it)->normVector();
             count ++;
           }
         }
         if ( count > 0 ){
           averageNorm /= count;
           addNodes.push_back( mesh.node( i ).pos() + averageNorm * dx );
         } else {
           addNodes.push_back( mesh.node( i ).pos() + ( averagePos - mesh.node( i ).pos() ).normalisedCopy() * dx );
         }
        //        addNodes.push_back( mesh.node( i ).pos() - RealPos(0.0, 0.0, dx ) );
       }
     }


    fstream file; openOutFile( outputName, &file );
    file.precision( 14 );
    file << addNodes.size() << endl;
    for ( uint i = 0; i < addNodes.size(); i ++ ){
      file << i << "\t" << addNodes[ i ][ 0 ] << "\t" <<  addNodes[ i ][ 1 ] << "\t" << addNodes[ i ][ 2 ] << endl;
    }

    file.close();
  }

  return 0;
}

/*
$Log: prepareMeshRefinement.cpp,v $
Revision 1.13  2010/06/22 14:42:47  thomas
*** empty log message ***

Revision 1.12  2010/06/18 10:17:59  carsten
win32 compatibility commit

Revision 1.11  2009/02/03 10:15:00  carsten
*** empty log message ***

Revision 1.10  2008/11/21 07:51:57  thomas
only changes in documentation

Revision 1.9  2008/02/14 12:46:41  carsten
*** empty log message ***

Revision 1.8  2008/02/11 11:16:32  carsten
*** empty log message ***

Revision 1.7  2008/01/17 17:49:55  carsten
*** empty log message ***

Revision 1.6  2007/12/13 18:42:16  carsten
*** empty log message ***

Revision 1.5  2007/04/04 18:35:04  carsten
*** empty log message ***

Revision 1.4  2006/12/18 16:38:55  carsten
*** empty log message ***

Revision 1.3  2006/01/31 18:27:58  carsten
*** empty log message ***

Revision 1.2  2006/01/22 21:02:06  carsten
*** empty log message ***

Revision 1.1  2005/11/17 15:52:07  carsten
*** empty log message ***

Revision 1.12  2005/06/21 14:53:59  carsten
*** empty log message ***

Revision 1.11  2005/06/13 16:00:00  carsten
*** empty log message ***

Revision 1.10  2005/06/01 18:26:51  carsten
*** empty log message ***

Revision 1.9  2005/05/31 10:50:04  carsten
*** empty log message ***

Revision 1.8  2005/05/03 15:55:34  carsten
*** empty log message ***

Revision 1.7  2005/04/07 11:41:08  carsten
*** empty log message ***

Revision 1.6  2005/03/20 22:51:59  carsten
*** empty log message ***

Revision 1.5  2005/03/11 17:49:11  carsten
*** empty log message ***

Revision 1.4  2005/02/18 14:06:19  carsten
*** empty log message ***

Revision 1.3  2005/02/16 19:36:38  carsten
*** empty log message ***

Revision 1.2  2005/02/10 14:07:59  carsten
*** empty log message ***

*/
