// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "constraintmat.h"


int create0thOrderConstraints( RDirectMatrix & C ){
  RVector vec( C.size(), 1.0 );
  return create0thOrderConstraints( C, vec );
}
int create0thOrderConstraints( RDirectMatrix & C, const RVector & vec ){
  C.clean();
  for ( int i = 0, imax = C.size(); i < imax; i ++ ){
    C.SetVal( i + 1, i + 1, 1 / vec[ i ] );
  }
  return 1;
}

vector < BaseElement * > findParaBoundarys( const BaseMesh & mesh, int marker ){
  vector < BaseElement * > bounds;

  int nBounds = mesh.boundaryCount();
  int leftParaId = 0, rightParaId = 0;

  for ( int i = 0; i < nBounds; i ++ ){
    leftParaId = -1;
    rightParaId = -1;

    if ( mesh.boundary( i ).marker() == marker ){
      if ( mesh.boundary( i ).leftCell() != NULL ) {
	leftParaId = (int)mesh.boundary( i ).leftCell()->attribute() - 2;
      }
      if ( mesh.boundary( i ).rightCell() != NULL ) {
	rightParaId = (int)mesh.boundary( i ).rightCell()->attribute() - 2;
      }

      if ( leftParaId > -1 || rightParaId > -1 ) {
	bounds.push_back( &mesh.boundary( i ) );
      }
    }
  }
  return bounds;
}

int create1stOrderConstraintsNew( SMatrix & C, const vector < BaseElement * > & bounds ){
  C.clear();

  int nBounds = bounds.size();
  int leftParaId = 0, rightParaId = 0;
  int boundCount = -1, colMax = -1;
  for ( int i = 0; i < nBounds; i ++ ){
    leftParaId = -1;
    rightParaId = -1;
    if ( bounds[ i ]->leftCell() != NULL ) leftParaId = (int)bounds[ i ]->leftCell()->attribute() - 2;
    if ( bounds[ i ]->rightCell() != NULL ) rightParaId = (int)bounds[ i ]->rightCell()->attribute() - 2;

    colMax= max( colMax, max( leftParaId, rightParaId ) );
    if ( leftParaId > -1 ) C.setVal( i, leftParaId, 1 );
    if ( rightParaId > -1 ) C.setVal( i, rightParaId, -1 );
  }

  C.setRows( nBounds );
  C.setCols( colMax + 1 );
  //C.save("C10.matrix", Ascii);

  return 1;

}

int create1stOrderConstraints( RDirectMatrix & C, const BaseMesh & meshPara){
  C.clean();
  SetpCells neighbours;
  double weight = 1.0;

  for ( int i = 0; i < meshPara.cellCount(); i ++ ){
    if ( meshPara.cell( i ).attribute() > 1 ){ 
      neighbours = meshPara.findNeighbours( meshPara.cell( i ) );
      if ( neighbours.size() == 0 ){ 
	cerr << WHERE_AM_I << "Can't determine neighbours. " << endl;
      } else {
	for ( SetpCells::iterator it = neighbours.begin(); it != neighbours.end(); it ++ ){
	  if ( (*it)->attribute() > 1 ){

	    BaseElement * tmpBound = const_cast<BaseMesh & >(meshPara).findBoundary( meshPara.cell( i ), *(*it) );
	    weight = 1.0;
	    if ( tmpBound != NULL ) {
	      if ( tmpBound->marker() > 1 ){ 
		// Boundary between meshPara.cell( i ) and neighbour is refraktor (marked by 2)
		cout << "cell: " << (int)meshPara.cell( i ).attribute() -1 
		     << " neighbour: " << (int)meshPara.cell( (*it)->id() ).attribute() -1<< " refraktor: " << tmpBound->marker() << endl;
		weight = 0.0;
		continue;
	      }
	    }
		  
// 	    cout << (int)meshPara.cell( i ).attribute() -1 << " " 
// 		 << " " << (*it)->id() << " " 
// 		 << (int)meshPara.cell( (*it)->id() ).attribute()<< endl;

	    C.SetVal( (int)(meshPara.cell( i ).attribute()) -1, 
		      (int)(meshPara.cell( (*it)->id() ).attribute()) -1, -weight );
	    C.AddVal( (int)(meshPara.cell( i ).attribute()) -1, 
		      (int)(meshPara.cell( i ).attribute()) -1, weight);
	  }
	}
      }
      //C.AddVal( (int)meshPara.cell( i ).attribute() -1, (int)meshPara.cell( i ).attribute() -1, 1);
    }
  }
//   //  multTranspose( Ctmp, C );

   C.save("C1.matrix");
   //   exit(0);
  return 1;
}

int create1stOrderConstraintsRobust( RDirectMatrix & C, const BaseMesh & meshPara, const RVector & model){
  C.clean();
  SetpCells neighbours;

  double sumAbs = 0.0, sumQuad = 0.0, modDiff = 0.0;
  int count = 0;

  for ( int i = 0; i < meshPara.cellCount(); i ++ ){
    if ( meshPara.cell( i ).attribute() > 1 ){ 
      neighbours = meshPara.findNeighbours( meshPara.cell( i ) );
      if ( neighbours.size() == 0 ){ 
	cerr << WHERE_AM_I << "Can't determine neighbours. " << endl;
      } else {
	for ( SetpCells::iterator it = neighbours.begin(); it != neighbours.end(); it ++ ){
	  if ( (*it)->attribute() > 1 ){

	    modDiff = model[ (int)meshPara.cell( i ).attribute() -2 ] -
	      model[ (int)meshPara.cell( (*it)->id() ).attribute() -2 ];
	    sumAbs += fabs( modDiff );
	    sumQuad += modDiff*modDiff;
	    count ++;
	  }
	}
      }
    }
  }

  for ( int i = 0; i < meshPara.cellCount(); i ++ ){
    if ( meshPara.cell( i ).attribute() > 1 ){ 
      neighbours = meshPara.findNeighbours( meshPara.cell( i ) );
      if ( neighbours.size() == 0 ){ 
	cerr << WHERE_AM_I << "Can't determine neighbours. " << endl;
      } else {
	for ( SetpCells::iterator it = neighbours.begin(); it != neighbours.end(); it ++ ){
	  if ( (*it)->attribute() > 1 ){
	    modDiff =  sumQuad / sumAbs / fabs( model[ (int)meshPara.cell( i ).attribute() -2 ] -
						model[ (int)meshPara.cell( (*it)->id() ).attribute() -2 ] ) / sumAbs * count;
	    //	    cout << modDiff << endl;
	    modDiff = min( modDiff, 1.0 );


	    C.AddVal( (int)meshPara.cell( i ).attribute() -1, 
		      (int)meshPara.cell( (*it)->id() ).attribute() -1, -modDiff );
	    C.AddVal( (int)meshPara.cell( i ).attribute() -1, 
		      (int)meshPara.cell( i ).attribute() -1, modDiff );
	  }
	}
      }
    }
  }
    C.save("CRobust.mat");
  return 1;
}

int createBoundaryWeightVector( const vector < BaseElement * > & bounds, RVector & boundWeight, double powz ){
  int nBounds = bounds.size();
  if ( nBounds != boundWeight.size() ){
    cerr << WHERE_AM_I << " nBounds = " << nBounds << " != " << boundWeight.size() << " boundWeight.size()" << endl;
    exit(-1);
  }
  int dim = 0;
  switch( bounds[ 0 ]->rtti() ){
  case MYMESH_EDGE_RTTI: dim = 2; break;
  case MYMESH_TRIANGLEFACE_RTTI: dim = 3; break;
  default: cerr << WHERE_AM_I << " boundary not spezified " << bounds[ 0 ]->rtti() << endl; 
    break;
  }

  for ( int i = 0; i < nBounds; i ++ ){
    boundWeight[ i ] = pow( 1 - abs( bounds[ i ]->normVector()[ dim - 1 ] ), powz ) + 0.1;
  }
return 1;
    
}

int create1stOrderConstraintsArea( RDirectMatrix & C, const BaseMesh & meshPara ){
  C.clean();
  SetpCells neighbours;
  double boundarySize = 0.0;
  RealPos boundaryNorm;
  vector < Node * > comNodes;
  BaseElement * boundary = NULL;
  double powerade = 1.0;

  for ( int i = 0; i < meshPara.cellCount(); i ++ ){
    if ( meshPara.cell( i ).attribute() > 1 ){
      neighbours = meshPara.findNeighbours( meshPara.cell( i ) );
      if ( neighbours.size() == 0 ){
	cerr << WHERE_AM_I << "Can't determine neighbours. " << endl;
      } else {
	for ( SetpCells::iterator it = neighbours.begin(); it != neighbours.end(); it ++ ){
	  if ( (*it)->attribute() > 1 ){

	    comNodes = commonNodes( meshPara.cell( i ), *(*it) );
	    
	    switch ( comNodes.size() ){
	    case 2: // wahrscheinlich eine edge
	      boundary = new Edge( *comNodes[ 0 ], *comNodes[ 1 ] );
	      break;
	    case 3: // wahrscheinlich ein triangleface
	      boundary = new TriangleFace( *comNodes[ 0 ], *comNodes[ 1 ], *comNodes[ 2 ] );
	      break;
	    default: 
	      cerr << WHERE_AM_I << " dont know what to do with: commonNodes.size() = " << comNodes.size() << endl;
	    }
	    boundarySize = boundary->area();
	    boundaryNorm = boundary->normVector();
	    delete boundary;
	    boundarySize = pow( 1 -  abs( boundaryNorm[ meshPara.dim() -1 ] ) , powerade ) + 0.1;

// 	    cout << boundarySize << endl;
// 	    cout << boundaryNorm << boundaryNorm.x() << ", " << boundaryNorm.y() << endl;

	    C.AddVal( (int)meshPara.cell( i ).attribute() -1,
		      (int)meshPara.cell( (*it)->id() ).attribute() -1, -boundarySize );
	    C.AddVal( (int)meshPara.cell( i ).attribute() -1,
		      (int)meshPara.cell( i ).attribute() -1, boundarySize);
	  }
	}
      }
    }
  }
  return 1;
}

int create2ndOrderDirichletConstraints( RDirectMatrix & C, const BaseMesh & meshPara ){
  C.clean();
  SetpCells neighbours;

  for ( int i = 0; i < meshPara.cellCount(); i ++ ){
    if ( meshPara.cell( i ).attribute() > 1 ){ 
      neighbours = meshPara.findNeighbours( meshPara.cell( i ) );
      if ( neighbours.size() == 0 ){ 
	cerr << WHERE_AM_I << "Can't determine neighbours. " << endl;
      } else {
	for ( SetpCells::iterator it = neighbours.begin(); it != neighbours.end(); it ++ ){
	  if ( (*it)->attribute() > 1 ){
	    C.AddVal( (int)meshPara.cell( i ).attribute() -1, 
		      (int)meshPara.cell( (*it)->id() ).attribute() -1, -2 );
	  }
	}
      }
      C.SetVal( (int)meshPara.cell( i ).attribute() -1, (int)meshPara.cell( i ).attribute() -1, 5);
    }
  }
  //  C.save("create2ndOrderDirichletConstraints.matrix");
  return 1;
}

int create2ndOrderNeumannConstraints( RDirectMatrix & C, const BaseMesh & meshPara ){
  C.clean();
  TO_IMPL
  SetpCells neighbours;

  for ( int i = 0; i < meshPara.cellCount(); i ++ ){
    if ( meshPara.cell( i ).attribute() > 1 ){ 
      neighbours = meshPara.findNeighbours( meshPara.cell( i ) );
      if ( neighbours.size() == 0 ){ 
	cerr << WHERE_AM_I << "Can't determine neighbours. " << endl;
      } else {
	for ( SetpCells::iterator it = neighbours.begin(); it != neighbours.end(); it ++ ){
	  if ( (*it)->attribute() > 1 ){
	    C.AddVal( (int)meshPara.cell( i ).attribute() -1, 
		      (int)meshPara.cell( (*it)->id() ).attribute() -1, -1 );
	  }
	}
      }
      C.SetVal( (int)meshPara.cell( i ).attribute() -1, (int)meshPara.cell( i ).attribute() -1, neighbours.size() );
    }
  }
  //  C.save("2ndOrderNeumannConstraints.matrix");
  return 1;
}

int create2ndOrderMixedConstraints( RDirectMatrix & C, const BaseMesh & meshPara ){
  C.clean();
  TO_IMPL

  SetpCells neighbours;

  for ( int i = 0; i < meshPara.cellCount(); i ++ ){
    if ( meshPara.cell( i ).attribute() > 1 ){ 
      neighbours = meshPara.findNeighbours( meshPara.cell( i ) );
      if ( neighbours.size() == 0 ){ 
	cerr << WHERE_AM_I << "Can't determine neighbours. " << endl;
      } else {
	for ( SetpCells::iterator it = neighbours.begin(); it != neighbours.end(); it ++ ){
	  if ( (*it)->attribute() > 1 ){
	    C.AddVal( (int)meshPara.cell( i ).attribute() -1, 
		      (int)meshPara.cell( (*it)->id() ).attribute() -1, -1 );
	  }
	}
      }
      C.SetVal( (int)meshPara.cell( i ).attribute() -1, (int)meshPara.cell( i ).attribute() -1, neighbours.size() );
    }
  }
  //  C.save("2ndOrderMixedConstraints.matrix");
  return 1;
}

// int create2ndOrderDirichletConstraints( RDirectMatrix & C, const BaseMesh & mesh, const BaseMesh & meshPara ){
//   RDirectMatrix Ctmp( C.size() );

//   //  fstream file; if ( !openInFile( paraMeshBody + ".neigh", &file) ) exit(-1);
// //   BaseMesh *pmesh;
// //   pmesh = new Mesh3D();
// //   pmesh->load( paraMeshBody + "Para" );
// //   pmesh->showInfos();
//   int nElement = 0, dummy = 0;
//   int n1 = 0, n2 = 0, n3 = 0, n4 = 0;
//   //  file >> nElement >> dummy;

//   int neighb[ 4 ];
//   for ( int i = 0; i < nElement; i ++ ){
//     //    file >> dummy >> n1 >> n2 >> n3 >> n4;
//     if ( meshPara.cell( i ).attribute() > 1 ){
//       neighb[0] = n1; neighb[1] = n2; neighb[2] = n3; neighb[3] = n4;

//       for ( int j = 0; j < 4; j ++ ){
//         if ( neighb[ j ] > -1 ){
//           if ( meshPara.cell( neighb[j] ).attribute() > 1 ){
//             // cout << i << "\t" <<  neighb[ j ] << " "<<pmesh.cell(neighb[j]).attribute()<< endl;
//             //C.AddVal( i+1, neighb[ j ]+1, -1 );
//             //C.AddVal( i+1, i+1, 2 );
//             Ctmp.AddVal( (int)meshPara.cell(i).attribute() -1,
//                       (int)meshPara.cell( neighb[ j ] ).attribute() -1, -1 );
//         //     C.AddVal( (int)pmesh.cell(i).attribute() -1,
// //                    (int)pmesh.cell(i).attribute() -1, 1 );
//           }
//         }
//       }
//      Ctmp.SetVal( (int)meshPara.cell(i).attribute() -1,
//                (int)meshPara.cell(i).attribute() -1, 4 );
//     }
//   }
//   //  multTranspose( Ctmp, C );

//   //  C.save("smoothnesConstraints2ndDirichlet.matrix");

//   //  file.close();
//   return 1;
// }

// int create2ndOrderMixedConstraints( RDirectMatrix & C, const BaseMesh & mesh, const BaseMesh & meshPara ){
//   RDirectMatrix Ctmp( C.size() );
//   //  fstream file; openInFile( paraMeshBody + ".neigh", &file);
  
//   TriangleFace *face;
// //   BaseMesh *pmesh;
// //   pmesh = new Mesh3D();
// //   pmesh->load( paraMeshBody + "Para" );
// //   pmesh->showInfos();
//   int nElement = 0, dummy = 0;
//   int n1 = 0, n2 = 0, n3 = 0, n4 = 0;
//   bool neumann = false;
//   //  file >> nElement >> dummy;
  
//   int neighb[ 4 ];
//   for ( int i = 0; i < nElement; i ++ ){
//     //    file >> dummy >> n1 >> n2 >> n3 >> n4;
//     if ( meshPara.cell( i ).attribute() > 1 ){ 
//       neighb[0] = n1; neighb[1] = n2; neighb[2] = n3; neighb[3] = n4;
      
//       for ( int j = 0; j < 4; j ++ ){
//         if ( neighb[ j ] > -1 ){
// 	  if ( meshPara.cell( neighb[j] ).attribute() > 1 ){
// 	    Ctmp.AddVal( (int)meshPara.cell(i).attribute() -1, 
// 			 (int)meshPara.cell( neighb[ j ] ).attribute() -1, -1 );
// 	  }
//       	}
//       }
//       neumann = false;
// //       face = dynamic_cast<Mesh3D*>(&meshPara)->findFace( pmesh->cell(i).node( 0 ), pmesh->cell(i).node( 1 ), 
// // 			      meshPara.cell(i).node( 2 ) );
// //       if ( face != NULL ) if ( face->marker() == -1 ) neumann = true;

// //       face = dynamic_cast<Mesh3D*>(pmesh)->findFace( pmesh->cell(i).node( 1 ), pmesh->cell(i).node( 2 ), 
// // 			     pmesh->cell(i).node( 3 ) );
// //       if ( face != NULL ) if ( face->marker() == -1 ) neumann = true;

// //       face = dynamic_cast<Mesh3D*>(pmesh)->findFace( pmesh->cell(i).node( 2 ), pmesh->cell(i).node( 3 ), 
// // 			     pmesh->cell(i).node( 0 ) );
// //       if ( face != NULL ) if ( face->marker() == -1 ) neumann = true;

// //       face = dynamic_cast<Mesh3D*>(pmesh)->findFace( pmesh->cell(i).node( 3 ), mesh.cell(i).node( 0 ), 
// // 			     pmesh->cell(i).node( 1 ) );
// //       if ( face != NULL ) if ( face->marker() == -1 ) neumann = true;
	 
// //       if ( neumann ){
// // 	Ctmp.SetVal( (int)pmesh->cell(i).attribute() -1, 
// // 		  (int)pmesh->cell(i).attribute() -1, 3 );
// //       } else {
// // 	Ctmp.SetVal( (int)pmesh->cell(i).attribute() -1, 
// // 		  (int)pmesh->cell(i).attribute() -1, 4 );
// //       }
//     }
//   }
//   //  multTranspose( Ctmp, C );
  
//   //  C.save("smoothnesConstraints2ndMixed.matrix");

//   //  file.close();
//   return 1;
// }

/*
$Log: constraintmat.cpp,v $
Revision 1.24  2007/05/15 19:59:17  thomas
*** empty log message ***

Revision 1.23  2007/03/07 16:38:14  carsten
*** empty log message ***

Revision 1.22  2006/12/21 17:22:56  carsten
*** empty log message ***

Revision 1.21  2006/09/25 11:31:49  carsten
*** empty log message ***

Revision 1.20  2006/09/22 11:16:52  carsten
*** empty log message ***

Revision 1.19  2006/09/22 11:05:28  carsten
*** empty log message ***

Revision 1.18  2006/09/21 17:08:40  carsten
*** empty log message ***

Revision 1.17  2006/04/05 19:30:07  carsten
*** empty log message ***

Revision 1.16  2006/03/01 17:25:14  carsten
*** empty log message ***

Revision 1.15  2006/02/14 11:09:18  carsten
*** empty log message ***

Revision 1.14  2006/02/07 15:17:22  thomas
new constraint formulation - first steps

Revision 1.13  2006/02/06 17:53:54  carsten
*** empty log message ***

Revision 1.12  2006/02/06 17:30:42  carsten
*** empty log message ***

Revision 1.11  2006/02/03 18:13:02  carsten
*** empty log message ***

Revision 1.10  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.9  2005/06/29 15:29:15  carsten
*** empty log message ***

Revision 1.8  2005/06/28 16:38:16  carsten
*** empty log message ***

Revision 1.7  2005/05/31 10:50:04  carsten
*** empty log message ***

Revision 1.6  2005/05/04 10:44:09  carsten
*** empty log message ***

Revision 1.5  2005/05/03 18:46:46  carsten
*** empty log message ***

Revision 1.4  2005/05/03 15:55:34  carsten
*** empty log message ***

Revision 1.3  2005/04/12 15:45:28  carsten
*** empty log message ***

Revision 1.1  2005/04/12 11:41:09  carsten
*** empty log message ***

*/
