// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "ldlwrapper.h"

#include <iostream>

using namespace std;
using namespace MyVec;

#ifdef HAVE_LDL
#ifdef __cplusplus
extern "C" {
extern void ldl_symbolic( int n, int Ap [ ], int Ai [ ], int Lp [ ],
			  int Parent [ ], int Lnz [ ], int Flag [ ], int P [ ], int Pinv [ ] ) ;

extern int ldl_numeric( int n, int Ap [ ], int Ai [ ], double Ax [ ],
			int Lp [ ], int Parent [ ], int Lnz [ ], int Li [ ], double Lx [ ],
			double D [ ], double Y [ ], int Pattern [ ], int Flag [ ],
			int P [ ], int Pinv [ ] ) ;

extern void ldl_lsolve( int n, double X [ ], int Lp [ ], int Li [ ], double Lx [ ] ) ;
extern void ldl_dsolve( int n, double X [ ], double D [ ]) ;
extern void ldl_ltsolve( int n, double X [ ], int Lp [ ], int Li [ ],  double Lx [ ] ) ;
extern void ldl_perm( int n, double X [ ], double B [ ], int P [ ] ) ;
extern void ldl_permt( int n, double X [ ], double B [ ], int P [ ] ) ;
extern int ldl_valid_perm( int n, int P [ ], int Flag [ ] ) ;
extern int ldl_valid_matrix( int n, int Ap [ ], int Ai [ ] ) ;
  //#include <ldl.h>
}
#endif // __cplusplus

#ifdef HAVE_AMD

#ifndef AMD_INFO
#define AMD_INFO 20
#endif

#ifndef AMD_OK
#define AMD_OK 0
#endif

#ifdef __cplusplus
extern "C" {
int amd_order( int n, const int Ap [ ], const int Ai [ ], int P [ ], double Control [ ], double Info [ ] ) ;
  //#include <amd.h>
}
#endif // __cplusplus
#endif //HAVE_AMD

#endif //HAVE_LDL

// void fill( const RDirectMatrix & A, taucs_ccs_matrix & ATaucs ){
//   int dim = A.size();

//   taucs_double * vals  = ATaucs.values.d;
//   int * rowIndex = ATaucs.rowind;
//   int * colPtr = ATaucs.colptr;
  
//   int k = 0, index = 0;
//   colPtr[ 0 ] = 0;
//   for ( int i = 0; i < dim; i++ ) {
//     for ( int j = 0, jmax = (long)A.mat[ i ][ 0 ]; j < jmax; j ++ ){
//       index = (long)(A.mat[ i ][ 2 * j + 1 ] - 1);
//       if ( index >= i ){
// 	rowIndex[ k ] = index;
// 	vals[ k ] = A.mat[ i ][ 2 * j + 2 ];
// 	k++;
//       }
//     }
//     colPtr[ i + 1 ] = k;
//   }

//   //  taucs_ccs_write_ijv(&ATaucs, "A.taucsmatrix" );
// }


LDLWrapper::LDLWrapper( const RDirectMatrix & S, bool verbose ) 
  : SolverWrapper( S, verbose ) {
#ifdef HAVE_LDL
  preordering_ = true;
  initialize_( S );
  factorise();

  return;
#endif //HAVE_LDL
  cerr << WHERE_AM_I << " LDL not installed" << endl;
}

LDLWrapper::~LDLWrapper(){
#ifdef HAVE_LDL
  if ( !dummy_ ){
    delete [] AcolPtr_;
    delete [] ArowIdx_;
    delete [] Avals_;

    delete [] Li_;
    delete [] Lp_;
    delete [] Lx_;
    delete [] D_;

    if ( preordering_ ){
      delete [] P_;
      delete [] Pinv_;
    }
  }
  return;
#endif //HAVE_LDL
  cerr << WHERE_AM_I << " LDL not installed" << endl;
}

int LDLWrapper::initialize_( const RDirectMatrix & S ){
#ifdef HAVE_LDL

  AcolPtr_ = new int[ dim_ + 1 ];
  ArowIdx_ = new int[ nVals_ ];
  Avals_ = new double[ nVals_ ];

  map < int, double > tmp;

  int k = 0;
  AcolPtr_[ 0 ] = 0;
  for ( int i = 0; i < dim_; i++ ){
    tmp = S.extractSparseRow( i );
    for ( map< int, double>::iterator it = tmp.begin(); it != tmp.end(); it ++){
      ArowIdx_[ k ] = it->first;
      Avals_[ k ] = it->second;
      k++;
    }
    AcolPtr_[ i + 1 ] = k;
  }

  if ( preordering_ ){
    P_ = new int[ dim_ ];
    Pinv_ = new int[ dim_ ];
#ifdef HAVE_AMD
    double Info[ AMD_INFO ];

//     int * RcolPtr = new int[ dim_ + 1 ];
//     int * RrowIdx = new int[ nVals_ ];
//     amd_preprocess (dim_, AcolPtr_, ArowIdx_, RcolPtr, RrowIdx);
//     AcolPtr_ = RcolPtr;
//     ArowIdx_ = RrowIdx;

    if ( amd_order( dim_, AcolPtr_, ArowIdx_, P_, (double *) NULL, Info) != AMD_OK ){
      cout << "AMD failed" << endl;
      exit(0);
    }
#else
    cout << "default preordering, i recongnise amd preorderung." << endl;
    for ( int i = 0; i < dim_; i ++ ) P_[ i ] = i;
#endif
  } else{
    P_ = (int *) NULL ;
    Pinv_ = (int *) NULL ;
  }

  return 1;
#endif //HAVE_LDL
  cerr << WHERE_AM_I << " LDL not installed" << endl;
  return 0;
}

int LDLWrapper::factorise(){
#ifdef HAVE_LDL
  Lp_ = new int[ dim_ + 1 ];
  int *Parent = new int[ dim_ ];
  int *Lnz = new int[ dim_ ];
  int *Flag = new int[ dim_ ];
  
  //  cout << "ldl_valid_matrix: " << ldl_valid_matrix ( dim_, AcolPtr_, ArowIdx_) << endl;

  ldl_symbolic( dim_, AcolPtr_, ArowIdx_, Lp_, Parent, Lnz, Flag, P_, Pinv_);
  int lnz = Lp_[ dim_ ];

  Li_ = new int[ lnz ];
  Lx_ = new double [ lnz ];
  D_ = new double[ dim_ ];

  double *Y = new double[ dim_ ];
  int *Pattern = new int[ dim_ ];

  int d = ldl_numeric( dim_, AcolPtr_, ArowIdx_, Avals_, Lp_, Parent, Lnz, Li_, Lx_, D_, Y, Pattern, Flag, P_, Pinv_) ;
  if ( d != dim_ ) {
    cout << WHERE_AM_I << " ldl_numeric failed, D (" << d << "," << d << ") is zero\n";
  }

  delete [] Y;
  delete [] Parent;
  delete [] Lnz;
  delete [] Flag;
  delete [] Pattern;

  return 1;
#endif //HAVE_LDL
  cerr << WHERE_AM_I << " LDL not installed" << endl;
  return 0;
}

int LDLWrapper::solve( const RVector & rhs, RVector & solution ){
#ifdef HAVE_LDL
  if ( dummy_ ) return 0;
  if ( solution.size() != dim_ ) solution.resize( dim_ );
  double * b = new double[ dim_ ];
  for ( int i = 0; i < dim_; i++) b[ i ] = rhs[ i ];

  if ( preordering_ ){
    double * bP = new double[ dim_ ];
    ldl_perm ( dim_, bP, b, P_) ;			/* y = Pb */
    ldl_lsolve( dim_, bP, Lp_, Li_, Lx_) ;
    ldl_dsolve( dim_, bP, D_) ;
    ldl_ltsolve( dim_, bP, Lp_, Li_, Lx_) ;
    ldl_permt ( dim_, b, bP, P_) ;			/* x = P'y */
    delete [] bP;
  } else {
    ldl_lsolve( dim_, b, Lp_, Li_, Lx_) ;
    ldl_dsolve( dim_, b, D_) ;
    ldl_ltsolve( dim_, b, Lp_, Li_, Lx_) ;
  }

  for ( int i = 0 ; i < dim_; i++) solution[ i ] = b[ i ];
//   solution.save("sol.vec", Ascii );
//   rhs.save("rhs.vec", Ascii );
//   exit(0);

  delete [] b;
  return 1;
#endif //HAVE_LDL
  cerr << WHERE_AM_I << " LDL not installed" << endl;
  return 0;
}

/*
$Log: ldlwrapper.cpp,v $
Revision 1.7  2006/11/21 19:26:29  carsten
*** empty log message ***

Revision 1.6  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.5  2005/06/02 15:44:10  carsten
*** empty log message ***

Revision 1.4  2005/02/07 13:33:13  carsten
*** empty log message ***

Revision 1.3  2005/01/14 11:40:24  carsten
*** empty log message ***

Revision 1.2  2005/01/11 20:02:57  carsten
*** empty log message ***

Revision 1.1  2005/01/11 19:24:10  carsten
*** empty log message ***


*/
