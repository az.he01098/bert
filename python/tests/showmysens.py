import pygimli as pg
import pybert as pb
import numpy as np
from pygimli.meshtools import appendTriangleBoundary
from pygimli.viewer import showMesh
from matplotlib import cm
import matplotlib.pyplot as plt

#%% create data
elpos = [ -6, -4, 4, 6 ]
data = pb.DataContainerERT()
for xe in elpos:
    data.createSensor( pg.RVector3( xe, 0., 0. ) )

data.resize(1)
data.createFourPointData(0,0,1,2,3)
print(data)

#%% create model (regular grid with triangle boundary)
dx = 0.5
x = np.arange( -10., 11., dx )
#z = np.arange( -7., .1, dx )
z = -np.arange( 0., 7.1, dx )
grid = pg.Mesh( 2 )
grid.create2DGrid( x, z )
#for xe in elpos:
#    ep = pg.RVector3( xe, 0.0, 0.0 )
#    for n in grid.nodes():
#        if n.pos().distance( ep ) < 0.1:
#            n.setMarker( -99 )
#for n in grid.nodes():
#    if n.y() == 0.0:
#        n.setMarker( -99 )

mesh = appendTriangleBoundary( grid, 200., 200., 10 )
print(mesh)
#showMesh( mesh )

#%% create forward operator and
f = pb.DCSRMultiElectrodeModelling( mesh, data, True )
f.region(10).setBackground( True )
f.createRefinedForwardMesh( )
res = pg.RVector( f.regionManager().parameterCount(), 100. )
resp = f.response( res )

f.createJacobian( res )
sensHom = pg.RVector( f.jacobian()[0] ) # make a copy
#showMesh( grid, sensHom, linear=True, cmap=cm.RdBu_r,
#         cMin=-.04, cMax=.04 )

#%% make anomalous body
ares = 500.
x1, x2, z1, z2 = -3, 3, -4, -2
for i, c in enumerate( grid.cells() ):
    xc, zc = c.center().x(), c.center().y()
    if xc>x1 and xc<x2 and zc>z1 and zc<z2:
        res[i] = ares
#showMesh( grid, res )

#%% calculate forward response and
resp = f.response( res )
f.createJacobian( res )
sensInhom = f.jacobian()[0]
sensInhomScal = sensInhom * res / resp[0]

#%% plot sensitivity
ax = showMesh( grid, sensInhomScal, linear=True, cmap=cm.RdBu_r,
         cMin=-.04, cMax=.04, showLater=True )
plt.plot( elpos, [0,0,0,0], 'kv', markersize=10 )
plt.plot( [x1,x2,x2,x1,x1], [z1,z1,z2,z2,z1],'k-' )
plt.savefig( 'sensScal' + str(ares) + '.eps', bbox_inches='tight' )
plt.show()