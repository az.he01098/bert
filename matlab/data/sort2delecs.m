function DataNew = sort2delecs(Data,islinear)

% SORT2DELECS - sort 2d electrode points linear or clock-wise
% Datanew = sort2delecs(Data) sorts by angle to midpoint
% sort2delec(Data,1) sorts by x

if nargin<2, islinear=0; end

if islinear,
    wi = Data.elec(:,1);
else
    mi = mean(Data.elec);
    mi(end) = mi(end) + 0.001;
    wi = atan2( Data.elec(:,2)-mi(2), Data.elec(:,1)-mi(1) );
end
[swi,ii] = sort(wi);
DataNew = Data;
DataNew.elec = Data.elec(ii,:);
jj = zeros( size(ii) );
jj(ii) = 1:length(jj);
fi=find(Data.a); DataNew.a(fi) = jj( Data.a(fi) );
fi=find(Data.b); DataNew.b(fi) = jj( Data.b(fi) );
fi=find(Data.m); DataNew.m(fi) = jj( Data.m(fi) );
fi=find(Data.n); DataNew.n(fi) = jj( Data.n(fi) );
